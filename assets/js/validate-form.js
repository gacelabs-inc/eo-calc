$(document).ready(function() {
	jQuery.validator.addMethod("emailExt", function(value, element, param) {
		return /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/.test(value);
	},'Your E-mail is wrong');

	var forms = $('.form-validate');

	forms.each(function(i, elem) {
		var form = $(elem);

		form.validate({
			ignore: '.ignore',
			errorPlacement: function(error, element) {
				if (element.prop("tagName") === 'SELECT' && element.hasClass('chosen-select')) {
					element.parent('div').find('.chosen-container-single').addClass('error');
				} else {
					element.parent('div').addClass('error');
				}
			},
			highlight: function (element, errorClass, validClass) {
				$(element).addClass('error');
				$(element).parent('div').addClass('error');
			},
			unhighlight: function (element, errorClass, validClass) {
				$(element).removeClass('error');
				$(element).parent('div').removeClass('error');
			},
			rules: oValidationRules, /*found on mainpage head tag*/
			submitHandler: function(form, e) {
				if ($(form).data('ajax')) {
					e.preventDefault();
					doAjax(form);
				} else {
					form.submit();
				}
			}
		});
	});
});

// revised and moved the codes from down here to public\requirements\js\recaptcha-loader.js