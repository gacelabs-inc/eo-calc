$(document).ready(function() {

	$(function() {
		$('#base-oil-volume').on('change', function() {
			var vol = $(this).val();

			$('.drop-count').each(function(i, e) {
				var origDropVal = $(e).attr('orig-drop-val'),
					totalDrops = origDropVal*vol;

				$(this).text(totalDrops);
				
			});
		});
	});

	$(function () {
		$('[data-toggle="popover"]').popover()
	})

});