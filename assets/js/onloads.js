$(document).ready(function(e) {
	var error = getParameterByName('error');
	var modal = getParameterByName('modal');
	var body_id = $('body').data('body-id')
	if (error) {
		var collection = $('#alert-collection');
		var clonedUI = collection.find('#alert-collection-danger').clone();
		clonedUI.find('span.msg').html(error);
		if (body_id == 'landing') {
			$('.content--search').prepend(clonedUI.fadeIn('slow'));
		} else {
			$('.content--'+body_id).prepend(clonedUI.fadeIn('slow'));
		}

	}
});