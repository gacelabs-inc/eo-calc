
var oAjax = false, doAjax = function(form, uploadFile) {
	if (typeof $.ajax == 'function') {
		if (form != undefined && form instanceof HTMLElement) {
			if (uploadFile == undefined) uploadFile = false;
			$('form').removeClass('active-ajaxed-form');

			var uiButtonSubmit = $(form).find('[type=submit]'),
			callbackFn = $(form).data('callback'),
			lastButtonUI = uiButtonSubmit.html(),
			oSettings = {
				url: form.action,
				type: form.method,
				dataType: 'jsonp',
				timeout: 1000,
				jsonpCallback: 'echo',
				beforeSend: function(xhr, settings) {
					$(form).addClass('active-ajaxed-form');
					uiButtonSubmit.attr('data-orig-ui', lastButtonUI);
					uiButtonSubmit.attr('disabled', 'disabled').html('<span class="spinner-border spinner-border-sm"></span> Processing ...');
				},
				success: function(data) {
					if (data && data.success) {
						console.log(data);
					}
				},
				error: function(xhr, status, thrown) {
					console.log(status, thrown);
				},
				complete: function(xhr, status) {
					uiButtonSubmit.html(uiButtonSubmit.data('orig-ui'));
					uiButtonSubmit.removeAttr('disabled');
					var fn = eval(callbackFn);
					if (typeof fn == 'function') {
						fn(form, xhr);
					}
				}
			};

			var formData = $(form).serialize();
			if (uploadFile) {
				formData = new FormData(form);
				oSettings.contentType = false;
				oSettings.processData = false;
			}

			oSettings.data = formData;
			// console.log(oSettings);
			if (oAjax != false) oAjax.abort();
			oAjax = $.ajax(oSettings);
		} else {
			console.log('form elements only!');
		}
	} else {
		console.log('ajax function not loaded!');
	}
}

/*jsonp functions*/
var reloadPage = function(data) {
	var msg = data.message == undefined ? 'Reloading page...' : data.message;
	var type = data.type == undefined ? 'info' : data.type;
	alert(msg);
	window.location.reload(true);
}

var alertBox = function(data) {
	$('form.active-ajaxed-form').find('div.alert').remove();
	var msg = data.message == undefined ? false : data.message;
	var type = data.type == undefined ? 'info' : data.type;
	var collection = $('#alert-collection');
	var clonedUI = collection.find('#alert-collection-'+type).clone();
	if (msg != false) {
		clonedUI.find('span.msg').html(msg);
		$('form.active-ajaxed-form').prepend(clonedUI.fadeIn('slow'));
	}
	// console.log(type, msg);
}

var getParameterByName = function(name, url) {
	if (url == undefined) url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';

    window.history.pushState({}, document.title, window.location.pathname);
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}
