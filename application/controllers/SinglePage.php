<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SinglePage extends MY_Controller {

	public function index()
	{
		$data = array(
			'metas' => array(
				// facebook opengraph
				'property="fb:app_id" content="INSERT APP ID"',
				'property="og:type" content="article"',
				'property="og:url" content="INSERT PAGE URL"',
				'property="og:title" content="INSERT PAGE TITLE"',
				'property="og:description" content="INSERT PAGE DESCRIPTION"',
				// SEO generics
				'name="description" content="INSERT PAGE DESCRIPTION"'
			),
			'css' => array(
				'head' => array('single-page'),
				'footer' => array()
			),
			'js' => array(
				'head' => array(),
				'footer' => array('main')
			),
			'body_class' =>array(camel_to_dashed(__CLASS__)),
			'content_top' => array(
				'content_modules/content--single-page'
			),
			'content_middle' => array(
				'content_modules/content--daily-blends'
			),
			'content_footer' => array(
			),
			'modals' => array(
				'advanced-search',
				'sign-up'
			),
			'page_data' => array(
				// user status
				'is_active' => 0
			)
		);

		$this->load->view('mainpage', $data);
	}
}
