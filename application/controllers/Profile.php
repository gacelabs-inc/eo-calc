<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends MY_Controller {

	public function index()
	{
		if (!$this->accounts->has_session) {
			redirect(base_url());
		}
		$data = array(
			'metas' => array(
				// facebook opengraph
				'property="fb:app_id" content="INSERT APP ID"',
				'property="og:type" content="article"',
				'property="og:url" content="INSERT PAGE URL"',
				'property="og:title" content="INSERT PAGE TITLE"',
				'property="og:description" content="INSERT PAGE DESCRIPTION"',
				// SEO generics
				'name="description" content="INSERT PAGE DESCRIPTION"'
			),
			'css' => array(
				'head' => array('profile'),
				'footer' => array()
			),
			'js' => array(
				'head' => array('app/'.strtolower(__CLASS__)),
				'footer' => array('main')
			),
			'body_class' =>array(camel_to_dashed(__CLASS__)),
			'content_top' => array(
				'content_modules/content--profile'
			),
			'content_middle' => array(
			),
			'content_footer' => array(
			),
			'modals' => array(
				'advanced-search'
			),
			'page_data' => array(
				// user status
				'is_active' => 1
			)
		);
		$this->load->view('mainpage', $data);
	}

	public function sign_in()
	{
		// $post = ['username'=>'bong', 'password'=>2];
		$post = $this->input->post();
		// debug($post, 1);
		$is_ok = $this->accounts->login($post, 'profile');
		// debug($is_ok, 1);
		$to = '/';
		if ($is_ok == false) $to = '?error=Invalid credentials';
		if ($this->accounts->has_session) {
			$to = 'profile?error=You are already signed in!';
		}
		redirect(base_url($to));
	}

	public function sign_up()
	{
		// $post = ['email_address'=>'leng2@gmail.com', 'password'=>23, 're_password'=>23];
		$post = $this->input->post();
		// debug($post, 1);
		$return = $this->accounts->register($post, 'profile'); /*this will redirect to settings page */
		// debug($this->session); debug($return, 1);
		if (isset($return['allowed']) AND $return['allowed'] == false) {
			if ($this->accounts->has_session) {
				redirect(base_url('profile?error='.$return['message']));
			} else {
				redirect(base_url('?error='.$return['message']));
			}
		}
	}

	public function recover()
	{
		if ($this->accounts->has_session) {
		} else {
			redirect(base_url());
		}
	}

	public function sign_out()
	{
		return $this->accounts->logout('/'); /*this will redirect to default page */
	}

	public function save($id=false)
	{
		if (!$this->accounts->has_session) {
			redirect(base_url());
		}
		$post = $this->input->post();
		// debug($id); debug($post, 1);
		if ($post) {
			$post['user_id'] = $this->accounts->profile['id'];
			if ($id == false) {
				$this->db->insert('profiles', $post);
			} else {
				$this->db->update('profiles', $post, ['id' => $id]);
			}
		}
		if ($this->input->is_ajax_request()) {
			do_jsonp_callback('alertBox', ['type'=>'success', 'message'=>'Profile saved!']);
		} else {
			redirect(base_url('profile?success=Profile saved!'));
		}
	}
}
