<div class="content--profile">
	<div class="container">
		<div class="row" style="margin-bottom: 30px;">
			<div class="col-md-12">
				<div class="bs-example bs-example-tabs" data-example-id="togglable-tabs">
					<ul class="nav nav-tabs" id="myTabs" role="tablist">
						<li role="presentation" class="active">
							<a href="#profile" id="profile-tab" role="tab" data-toggle="tab" aria-controls="profile" aria-expanded="true">
								<h4 class="zero-gap"><i class="fa fa-user-circle-o i-left"></i><span>Profile</span></h4>
							</a>
						</li>
						<li role="presentation" class="">
							<a href="#new-blend" role="tab" id="pronew-blendfile-tab" data-toggle="tab" aria-controls="new-blend" aria-expanded="false">
								<h4 class="zero-gap"><i class="fa fa-flask i-left"></i><span>New blend</span></h4>
							</a>
						</li>
					</li>
				</ul>

				<div class="tab-content" id="profile-page-tab-container">
					<div class="tab-pane fade active in" role="tabpanel" id="profile" aria-labelledby="profile-tab">
						<div class="row">
							<form class="col-md-6 form-validate" data-ajax='true' data-callback="updateProfileNav" method="post" action="/profile/save/<?php echo $profile_info['id'];?>">
								<div class="form-group col-md-6">
									<label for="firstname" style="display: block;">
										<small>First name</small>
										<input type="text" id="firstname" name="firstname" class="form-control" style="margin-top: 5px;" value="<?php echo $profile_info['firstname'];?>">
									</label>
								</div>
								<div class="form-group col-md-6">
									<label for="lastname" style="display: block;">
										<small>Last name</small>
										<input type="text" id="lastname" name="lastname" class="form-control" style="margin-top: 5px;" value="<?php echo $profile_info['lastname'];?>">
									</label>
								</div>

								<div class="clearfix"></div>

								<div class="form-group col-md-12">
									<label for="country" style="display: block;">
										<small>Country</small>
										<select id="country" name="country_id" class="form-control" style="margin-top: 5px;" onmousedown="if(this.options.length>3){this.size=3;}"  onchange='this.size=0;' onblur="this.size=0;">
											<!-- populated from db -->
											<?php foreach (get_data_in('countries') as $key => $country): ?>
												<option value="<?php echo $country['id'];?>"
												<?php echo $profile_info['country_id'] == $country['id'] ? 'selected' : '';?>><?php echo $country['name'];?></option>
											<?php endforeach ?>
										</select>
									</label>
								</div>

								<div class="col-md-6">
									<button type="submit" class="btn btn-success">Save</button>
								</div>
							</form>
						</div>
					</div>
					<div class="tab-pane fade" role="tabpanel" id="new-blend" aria-labelledby="new-blend-tab">
						<div class="row">
							<form class="col-md-6 form-validate" data-ajax='true' method="post" action="/blend/save/">
								<div class="col-md-12" style="margin-bottom: 15px;">
									<div class="input-group">
										<span class="input-group-addon">
											<i class="fa fa-info-circle i-left" tabindex="0" role="button" data-toggle="popover" data-placement="bottom" data-trigger="focus" title="Blend name" data-content="A good blend name is a catchy one. Come up something like Sneeze Away or Colds Buster or Body Pain Soothing Relief. Go a head and sqeeze out your creativity!"></i><span>Blend name</span>
										</span>
										<input type="text" id="name" name="name" class="form-control" aria-describedby="blend-name" required="required">
									</div>
								</div>

								<div class="col-md-12" style="margin-bottom: 15px;">
									<div class="input-group">
										<span class="input-group-addon">
											<i class="fa fa-info-circle i-left" tabindex="0" role="button" data-toggle="popover" data-placement="bottom" data-trigger="focus" title="Description" data-content="Say a little something about your new blend. Give out tips, provide the benefits or maybe how to's to make the blend perfect and effective."></i><span>Description</span>
										</span>
										<input type="text" id="description" name="description" class="form-control" aria-describedby="blend-description" required="required">
									</div>
								</div>

								<div class="col-md-12" style="margin-bottom: 15px;">
									<div class="input-group">
										<input type="text" class="form-control" placeholder="Search for oil...">
										<span class="input-group-btn">
											<button class="btn btn-primary" type="button"><i class="fa fa-check"></i></button>
										</span>
									</div>
								</div>

								<div class="col-md-6">
									<button type="submit" class="btn btn-success">Save Blend</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>