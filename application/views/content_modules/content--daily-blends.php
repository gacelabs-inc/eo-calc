<div class="content--daily-blends">
	<div class="container">
		<div class="row" style="margin-bottom: 30px;">
			<div class="col-md-12">
				<h3 class="zero-gap">Timely blends</h3>
			</div>
		</div>

		<div class="row">
			<!-- foreach -->
			<div class="col-md-4 col-sm-4 col-xs-6">
				<div class="panel panel-default">
					<div class="panel-body panel-bg" style="background-image: url(<?php echo base_url('assets/images/eo-sample1.jpg'); ?>)"></div>
					<div class="panel-body">
						<div class="panel-body--header">
							<a href="single-page"><h4 class="panel-body--blend-title">Blend title</h4></a>
						</div>
						<div class="panel-body--subheader" style="margin-bottom: 5px;">
							<ul class="li-capsule theme-bg">
								<li><small>Stuffy nose</small></li>
								<li><small>Colds</small></li>
							</ul>
						</div>
						<div class="panel-body--subheader">
							<ul class="li-capsule">
								<li><small>Diffuse</small></li>
								<li><small>Rub</small></li>
								<li><small>Soap</small></li>
							</ul>
						</div>
					</div>
					<div class="panel-footer">
						<ul class="spaced-list around" id="panel-activities">
							<li><i class="fa fa-facebook i-left"></i>42</li>
							<li><i class="fa fa-heart i-left"></i>10</li>
							<li><i class="fa fa-fire i-left"></i>501</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>