<div class="content--search">
	<div class="container">
		<div class="row">
			<div class="valign-parent">
				<div class="col-md-8 col-sm-7 col-xs-12">
					<div class="slogan-box">
						<h1 class="hero-title"><b>There's a blend for that!</b></h1>
						<h2>Hundreds of proven effective essential oil blends for you and your loved ones.</h2>
					</div>
					<form id="main-search-form">
						<div class="input-group">
							<span class="input-group-addon" id="blend-prefix"><b>Blend for</b></span>
							<input type="text" class="form-control" placeholder="stuffy nose" aria-describedby="blend-prefix">
							<span class="input-group-btn">
								<button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
							</span>
						</div>
					</form>
					<button class="btn btn-sm btn-default" id="advanced-search" data-toggle="modal" data-target="#advanced-search-modal"><i class="fa fa-sliders i-left"></i>Advanced search</button>
				</div>

				<div class="col-md-4 col-sm-3 hidden-xs"></div>
			</div>
		</div>
	</div>
</div>