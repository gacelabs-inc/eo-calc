<div class="content--single-page">
	<div class="container">
		<div class="row">
			<div class="col-md-12" style="margin-bottom: 20px;">
				<h1 class="zero-gap"><b>Blend title goes here</b></h1>
				<h4>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
				</h4>
			</div>

			<div class="col-md-6 col-sm-6 col-xs-12 blend-ingredients-table">
				<div class="blend-list-parent">
					<ul class="inline-list blend-list--ul">
						<li><h3 class="zero-gap"><b>Carrier</b></h3></li>
						<li>
							<div class="form-group">
								<select class="form-control" id="base-oil-volume">
									<option value="1">50 ml</option>
									<option value="2">100 ml</option>
									<option value="3">150 ml</option>
								</select>
							</div>
						</li>
					</ul>
					<ul class="inline-list blend-list--ul">
						<li><h3 class="zero-gap">Nutmeg</h3></li>
						<li>
							<h3 class="zero-gap">
								<span class="drop-count" orig-drop-val="3">3</span></i><i class="fa fa-tint i-right"></i>
							</h3>
						</li>
					</ul>
					<ul class="inline-list blend-list--ul">
						<li><h3 class="zero-gap">Eucalyptus</h3></li>
						<li>
							<h3 class="zero-gap">
								<span class="drop-count" orig-drop-val="5">5</span><i class="fa fa-tint i-right"></i>
							</h3>
						</li>
					</ul>
					<ul class="inline-list blend-list--ul">
						<li><h3 class="zero-gap">Lavender</h3></li>
						<li>
							<h3 class="zero-gap">
								<span class="drop-count" orig-drop-val="4">4</span><i class="fa fa-tint i-right"></i>
							</h3>
						</li>
					</ul>
					<ul class="inline-list blend-list--ul">
						<li><h3 class="zero-gap">Peppermint</h3></li>
						<li>
							<h3 class="zero-gap">
								<span class="drop-count" orig-drop-val="2">2</span><i class="fa fa-tint i-right"></i>
							</h3>
						</li>
					</ul>
				</div>
			</div>

			<div class="col-md-6 col-sm-6 col-xs-12">
				<div style="margin-bottom: 25px;">
					<h4>Best for:</h4>
					<ul class="li-capsule theme-bg">
						<li>Stuffy nose</li>
						<li>Colds</li>
						<li>Fever</li>
					</ul>
				</div>
				<div style="margin-bottom: 25px;">
					<h4>Usage:</h4>
					<ul class="li-capsule">
						<li>Diffuse</li>
						<li>Rub</li>
						<li>Soap</li>
					</ul>
				</div>
				<div>
					<ul class="inline-list" id="panel-activities">
						<li style="margin-right: 10px;"><i class="fa fa-facebook i-left"></i>42</li>
						<li style="margin-right: 10px;"><i class="fa fa-heart i-left"></i>10</li>
						<li><i class="fa fa-fire i-left"></i>501</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>