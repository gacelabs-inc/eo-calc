<nav class="navbar navbar-default navbar-fixed-top">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-form-collapse" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="<?php echo base_url(); ?>"><b>EO Blends</b></a>
		</div>
		
		<div class="collapse navbar-collapse" id="navbar-form-collapse">
			<ul class="nav navbar-nav navbar-right">
				<li><a href="#"><b style="color:#f71e1e;">Submit a blend and win!</b></a></li>
				<?php if ($current_profile == false) : ?>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Sign Up/ Log In <span class="caret"></span></a>
						<div class="dropdown-menu">
							<form class="navbar-form form-validate" method="post" action="/sign-in/">
								<p><b>Welcome back!</b></p>
								<div class="form-group">
									<input type="email" name="email_address" class="form-control" placeholder="johndoe@domain.com">
								</div>
								<div class="form-group">
									<input type="password" name="password" class="form-control" placeholder="********">
								</div>
								<div class="form-group">
									<button class="btn btn-block btn-primary">Log in</button>
								</div>
								<a href="#"><small>Reset password</small></a>
							</form>
							<div class="hr-center-text"><span>Or</span></div>
							<div class="navbar-panel">
								<p>Sign up <a href="#" data-toggle="modal" data-target="#sign-up-modal">here.</a></p>
							</div>
						</div>
					</li>
				<?php else : ?>
					<li><a href="profile"><i class="fa fa-user-circle-o i-left"></i><font id="usersname"><?php echo $profile_info['firstname'];?></font></a></li>
					<li><a href="/sign-out/"><i class="fa fa-sign-out"></i><span class="hidden-lg hidden-md hidden-sm i-right">Sign Out</span></a></li>
				<?php endif; ?>
			</ul>
		</div>
	</div>
</nav>