<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<?php $this->load->view('page_requires/head'); ?>

		<script type="text/javascript"><?php /*set valid form fields by calling this function in this current controller $this->set_form_valid_fields(ARRAY OF FIELDS REQUIRED)*/ ?>var oValidationRules=<?php echo json_encode($this->session->userdata('valid_fields'));?>;</script>
	</head>

	<body class="<?php foreach($body_class as $value) { echo trim($value)." ";} ?>" data-body-id="<?php echo $this->class_name?>">

		<div id="content-wrapper">
		
			<section id="navbar-section">
				<?php $this->load->view('navbar_modules/navbar'); ?>
			</section>

			<div id="content-body-wrapper">
				
				<section id="content--top">
					<?php
						foreach ($content_top as $value) {
							$this->load->view($value);
						}
					?>
				</section>

				<section id="content--middle">
					<?php
						foreach ($content_middle as $value) {
							$this->load->view($value);
						}
					?>
				</section>

				<section id="content--footer">
					<?php
						foreach ($content_footer as $value) {
							$this->load->view($value);
						}
					?>
				</section>

			</div>

		</div>

		<?php
			foreach ($css as $key => $values) {
				if ($key == "footer") {
					foreach ($values as $value) {
						echo '<link rel="stylesheet" type="text/css" href="'.base_url('assets/css/'.$value.'').'.css">';
						echo "\r\n";
					}
				}
			}
		?>

		<script type="text/javascript" src="<?php echo base_url('assets/js/jquery-min.js'); ?>"></script>
		<script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
		<script type="text/javascript" src="<?php echo base_url('assets/js/custom.js');?>"></script>
		<script type="text/javascript" src="<?php echo base_url('assets/js/onloads.js');?>"></script>
		<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.validate.min.js');?>"></script>
		<script type="text/javascript" src="<?php echo base_url('assets/js/validate-form.js');?>"></script>
		<?php
			foreach ($js as $key => $values) {
				if ($key == "footer") {
					foreach ($values as $value) {
						echo '<script type="text/javascript" src="'.base_url('assets/js/'.$value.'').'.js"></script>';
						echo "\r\n";
					}
				}
			}
		?>

		<?php
			foreach ($modals as $value) {
				$this->load->view('modals/'.$value.'');
			}
		?>		

	</body>

	<div id="alert-collection" class="hide">
		<div class="alert alert-success alert-dismissible" id="alert-collection-success" role="alert">
			<span class="msg">This is a success alert—check it out!</span>
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
		<div class="alert alert-danger alert-dismissible" id="alert-collection-danger" role="alert">
			<span class="msg">This is a danger alert—check it out!</span>
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
		<div class="alert alert-warning alert-dismissible" id="alert-collection-warning" role="alert">
			<span class="msg">This is a warning alert—check it out!</span>
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
		<div class="alert alert-info alert-dismissible" id="alert-collection-info" role="alert">
			<span class="msg">This is a info alert—check it out!</span>
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
	</div>
</html>