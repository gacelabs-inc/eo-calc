<?php if ($current_profile == false) : ?>
<div class="modal fade" id="sign-up-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<form method="post" action="/sign-up/" id="sign-up-form" class="form-validate">
					<div class="form-group">
						<label for="email" style="display: block;">
							<small>Email</small>
							<input type="email" id="email" name="email_address" class="form-control" placeholder="johndoe@domain.com" style="margin-top: 5px;">
						</label>
					</div>
					<div class="form-group">
						<label for="password" style="display: block;">
							<small>Password</small>
							<input type="password" id="password" name="password" class="form-control" placeholder="At least 8 characters" style="margin-top: 5px;">
						</label>
					</div>
					<div class="form-group">
						<label for="re-password" style="display: block;">
							<small>Retype password</small>
							<input type="password" id="re-password" name="re_password" class="form-control" placeholder="Confirm password" style="margin-top: 5px;">
						</label>
					</div>
					<button type="submit" class="btn btn-success btn-block" style="margin-bottom: 5px;">Sign Up<i class="fa fa-angle-right i-right"></i></button>
				</form>
			</div>
		</div>
	</div>
</div>
<?php endif; ?>