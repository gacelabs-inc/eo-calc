<div class="modal fade" id="advanced-search-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel"><i class="fa fa-sliders i-left"></i>Advanced Search</h4>
			</div>
			<div class="modal-body">
				<p>Blend with the following oils:</p>
				<div class="input-group">
					<input type="text" class="form-control" placeholder="Search for oil...">
					<span class="input-group-btn">
						<button class="btn btn-primary" type="button"><i class="fa fa-check"></i></button>
					</span>
				</div>
				<div class="checkbox">
					<label>
						<input type="checkbox" value="" checked> Nutmeg
					</label>
				</div>
				<div class="checkbox">
					<label>
						<input type="checkbox" value="" checked> Eucalyptus
					</label>
				</div>
				<div class="checkbox">
					<label>
						<input type="checkbox" value="" checked> Lavender
					</label>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success" data-dismiss="modal"><i class="fa fa-flask i-left"></i>Blend</button>
			</div>
		</div>
	</div>
</div>