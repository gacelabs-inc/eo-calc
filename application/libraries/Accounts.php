<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Accounts {

	private $class = FALSE; 
	public $has_session = FALSE; 
	public $profile = FALSE;
	public $device_id = FALSE;

	public function __construct()
	{
		$this->class =& get_instance();
		$this->has_session = $this->class->session->userdata('profile') ? TRUE : FALSE;
		$this->profile = $this->class->session->userdata('profile');
	}

	public function check_credits($credits=FALSE, $table='users', $function=FALSE)
	{
		$allowed = FALSE; $user = FALSE; $msg = '';
		if ($credits) {
			if (isset($credits['email_address']) AND isset($credits['password'])) {
				$credits['password'] = md5($credits['password']);
				$email_address_query = $this->class->db->get_where($table, ['email_address' => $credits['email_address']]);

				$enter = FALSE;
				if ($email_address_query->num_rows() > 0) {
					if ($function == 'register') {
						$enter = $allowed = TRUE;
					} elseif ($function == 'login') {
						$enter = TRUE;
					}
				}
				// debug($enter, 1);

				if ($enter) {
					$query = $this->class->db->get_where($table, $credits);
					// debug($query->row_array(), 1);
					if ($query->num_rows()) {
						$allowed = TRUE;
						$user = $query->row_array();
					} else {
						$msg = 'Invalid password!';
					}
				} else {
					$msg = $function == 'login' ? 'Email address does not exist!' : 'Email address already exist!';
				}
			}
		}

		return ['allowed' => $allowed, 'message' => $msg, 'profile' => $user];
	}

	public function register($post=FALSE, $redirect_url='', $table='users')
	{
		$allowed = FALSE; $user = FALSE;; $passed = TRUE; $msg = '';
		if ($post) {
			// debug($post, 1);
			if (isset($post['password']) AND isset($post['re_password'])) {
				if ($post['re_password'] !== $post['password']) {
					$passed = FALSE;
					$msg = 'Password mismatch!';
				}
			}
			if (isset($post['email_address']) AND (isset($post['password']) AND strlen(trim($post['password'])) > 0)) {
				$credits = ['email_address'=>$post['email_address'], 'password'=>$post['password']];
				$return = $this->check_credits($credits, $table, __FUNCTION__);
				// debug(isset($return['allowed']) AND $return['allowed'] == FALSE, 1);
				if ($passed) {
					if (isset($return['allowed']) AND $return['allowed'] == FALSE) {
						$post['password'] = md5($post['password']);
						$query = $this->class->db->insert($table, $post);
						$id = $this->class->db->insert_id();
						// debug($id);
						if ($id) {
							$msg = '';
							$allowed = TRUE;
							$qry = $this->class->db->get_where($table, ['id' => $id]);
							$user = $qry->row_array();
							// debug($user, 1);
							unset($user['password']);
							unset($user['re_password']);
							$this->class->session->set_userdata('profile', $user);
							$this->profile = $user;
						}
						if ($redirect_url != '') {
							redirect(base_url($redirect_url == 'home' ? '' : $redirect_url));
						}
					} else {
						$msg = 'Account already exist! Try signing in';
					}
				}
			} else {
				if (isset($post['email_address']) AND empty(trim($post['email_address']))) {
					$msg = 'Email address is required!';
				} elseif (isset($post['password']) AND empty(trim($post['password']))) {
					$msg = 'Password is required!';
				} else {
					$msg = 'Email address and password are required!';
				}
			}
		} else {
			$msg = 'Empty request found!';
		}

		return ['allowed' => $allowed, 'message' => $msg, 'profile' => $user];
	}

	public function login($credits=FALSE, $redirect_url='', $table='users')
	{
		// debug($credits, 1);
		if ($credits != FALSE AND is_array($credits) AND $this->has_session == FALSE) {
			/*user is logging in*/
			$return = $this->check_credits($credits, $table, __FUNCTION__);
			// debug($return, 1);
			if (isset($return['allowed']) AND $return['allowed']) {
				unset($return['profile']['password']);
				unset($return['profile']['re_password']);
				$this->class->session->set_userdata('profile', $return['profile']);
				$this->profile = $return['profile'];
				if ($redirect_url != '') {
					redirect(base_url($redirect_url == 'home' ? '' : $redirect_url));
				} else {
					return TRUE;
				}
			}
		}
		/*else the user is logged in or session active*/
		return FALSE;
	}

	public function logout($redirect_url='')
	{
		$profile = $this->class->session->userdata('profile');
		$this->class->session->unset_userdata('profile');
		$this->class->session->sess_destroy();
		$this->profile = FALSE;
		$this->has_session = FALSE;
		// $this->class->pushthru->trigger('logout-profile', 'browser-'.$this->device_id.'-sessions-logout', $profile);
		redirect(base_url($redirect_url == 'home' ? '' : $redirect_url));
	}

	public function refetch()
	{
		$user = $this->class->db->get_where('users', ['id' => $this->profile['id']]);
		if ($user->num_rows()) {
			$request = $user->row_array();
			unset($request['password']);
			unset($request['re_password']);

			$request['info'] = $this->assemble_table_fields('profiles');
			$info = $this->class->db->get_where('profiles', ['user_id' => $this->profile['id']]);
			if ($info->num_rows() > 0) {
				$request['info'] = $info->row_array();
			}
			// debug($request, 1);
			$this->class->session->set_userdata('profile', $request);
			$this->profile = $request;
			$this->device_id = format_ip();
			// debug($this, 1);
			return $this->profile;
		}
		return FALSE;
	}

	private function assemble_table_fields($table=FALSE)
	{
		if ($table) {
			$table_fields = $this->class->db->field_data($table);
			$field_data = [];
			foreach ($table_fields as $key => $field) {
				$field_data[$field->name] = FALSE;
			}
			// debug($field_data, 1);
			return $field_data;
		}
		throw new Exception("Add profiles table in DB", 1);
		
	}
}